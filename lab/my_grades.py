# -*- encoding: utf-8 -*-

import getpass
import requests
from bs4 import BeautifulSoup

url = {
    'login': 'https://e.sggw.waw.pl/login/index.php',
    'grades': 'http://e.sggw.waw.pl/grade/report/user/index.php?id=649'#'http://e.sggw.waw.pl/grade/report/user/index.php?id=564'649
}

#username = raw_input('Użytkownik: ')
#password = getpass.getpass('Hasło: ')
payload = {
    'username': '***',
    'password': '***'
}

# Logowanie
session = requests.session()
session.post('https://e.sggw.waw.pl/login/index.php',
                 data=payload, verify=False)

# Pobranie strony z ocenami
response = session.get(
    'http://e.sggw.waw.pl/grade/report/user/index.php?id=649', verify=False)

# Odczyt strony
html = response.text

# Parsowanie strony
parsed = BeautifulSoup(html)

# Scraping
grades = parsed.find('table', 'user-grade')

# Sortowanie
img = grades.find_all('tr')
#img1 = grades.find_all('img', title='Kategoria')
#cat1 = img1.find_parent('td').text
#img2 = grades.find_all('img', title='Zadanie')
#cat2 = img2.find_parent('td').text
#ocena = grades.find_all('td', class_='  item b1b')[1].text

tablica = []

def Sortuj(argument):
    tabela = []
    aa = 0
    for ite in argument:
        tes = ite.find_parent('tr').find_all('td', class_='  item b1b')[2].text
        tesa = tes[5:].split(',')[0]
        try:
            aaa = int(tesa)
        except ValueError:
            aaa = int(tes[4:].split(',')[0])
        if (aaa > aa):
            aa = aaa
    while (aa >= 0):
        temp = 1
        tabela = []
        for items in argument:
            temp = 1
            try:
                x = int(items.find_parent('tr').find_all('td', class_='  item b1b')[1].text.split(',')[0])
            except ValueError:
                temp = 2
                tabela.append(items)
            if (temp == 1):
                if (x == aa):
                    print '- '+items.find_parent('td').text+' '+items.find_parent('tr').find_all('td', class_='  item b1b')[1].text
        if (aa == 0):
            for itema in tabela:
                print '- '+itema.find_parent('td').text+' '+itema.find_parent('tr').find_all('td', class_='  item b1b')[1].text
        aa=aa-1

# Wyświetlenie posortowanych ocen w kategoriach
for item in img:
    for item2 in item.find_all('img', title='Kategoria'):
        Sortuj(tablica)
        tablica = []
        print item2.find_parent('td').text
    for item2 in item.find_all('img', title='Zadanie'):
        tablica.append(item2)
    for item2 in item.find_all('img', title='Warsztaty'):
        tablica.append(item2)
    for item2 in item.find_all('img', title='Forum'):
        tablica.append(item2)

Sortuj(tablica)