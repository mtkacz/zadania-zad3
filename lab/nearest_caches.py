# -*- encoding: utf-8 -*-
import json
import requests
from pprint import pprint

address = raw_input('Podaj adres: ')
# Znalezienie współrzędnych adresu
baseAddressFromGoogle = 'http://maps.googleapis.com/maps/api/geocode/json'
baseAddressOkapi = 'http://opencaching.pl/okapi/services/caches/geocaches'
baseAddressGoogleFromOkapi = 'http://maps.googleapis.com/maps/api/geocode/json'

dataAddresFromGoogle = {'address': address, 'sensor': 'false' }
response = requests.get(baseAddressFromGoogle, params=dataAddresFromGoogle)
if response.json()[u'status'] =='OK':
    # Znalezienie najbliższych geoskrytek
    dataFromOkapi = {'center': str(response.json()[u'results'][0][u'geometry'][u'location'][u'lat']) + '|' +
                               str(response.json()[u'results'][0][u'geometry'][u'location'][u'lng']), 'consumer_key': 'bgMhAch2UUbBDPZ5g7hM' }

    # Wydobycie informacji o lokalizacji skryteks
    caches = '|'.join((requests.get('http://opencaching.pl/okapi/services/caches/search/nearest', params=dataFromOkapi)).json()['results'])
    dataAddresFromOkapi = { 'cache_codes':caches , 'consumer_key': 'bgMhAch2UUbBDPZ5g7hM' }
    dataToListOkapi = requests.get(baseAddressOkapi, params=dataAddresFromOkapi)

    # Przydzielenie adresu skrytkom na podstawie ich współrzędnych
    listsOfUnavailable = []
    listsOFTemporarilyUnavailable = []
    listsOFAvailable = []
    for itemDataToListOkapi in dataToListOkapi.json().keys():
        tab = dataToListOkapi.json()[itemDataToListOkapi][u'location'].split('|')
        dataGoogle = {'latlng': tab[0] + ',' + tab[1], 'sensor': 'false' }
        res = requests.get(baseAddressGoogleFromOkapi, params=dataGoogle)
        #nie wszystkie lokalizacje znajduje google dlatego dodane try, wrazie nie znalezienia podaje tylko wspolzedne
        try:
            if dataToListOkapi.json()[itemDataToListOkapi][u'status'] == 'Available':
                listsOFAvailable.append(dataToListOkapi.json()[itemDataToListOkapi][u'name'] + res.json()[u'results'][0][u'formatted_address'])
            elif dataToListOkapi.json()[itemDataToListOkapi][u'status'] == 'Archived':
                listsOfUnavailable.append(dataToListOkapi.json()[itemDataToListOkapi][u'name'] + res.json()[u'results'][0][u'formatted_address'])
            else:
                listsOFTemporarilyUnavailable.append(dataToListOkapi.json()[itemDataToListOkapi][u'name'] + res.json()[u'results'][0][u'formatted_address'])
        except:
            if dataToListOkapi.json()[itemDataToListOkapi][u'status'] == 'Available':
                listsOFAvailable.append(dataToListOkapi.json()[itemDataToListOkapi][u'name'] + tab[0] + ',' + tab[1])
            elif dataToListOkapi.json()[itemDataToListOkapi][u'status'] == 'Archived':
                listsOfUnavailable.append(dataToListOkapi.json()[itemDataToListOkapi][u'name'] + tab[0] + ',' + tab[1])
            else:
                listsOFTemporarilyUnavailable.append(dataToListOkapi.json()[itemDataToListOkapi][u'name'] + tab[0] + ',' + tab[1])

    # Wyświetlenie wyniku
    print('Dostepne')
    for item in listsOFAvailable:
        print item
    print('Niedostepne')
    for item in listsOfUnavailable:
        print item
    print('Tymczasowo niedostepne')
    for item in listsOFTemporarilyUnavailable:
        print item
else:
    print 'Podales bledne dane lub nieistniejaca ulice, jednym slowiem nie ma cie w google'