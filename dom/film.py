# -*- encoding: utf-8 -*-

import getpass
import requests
import urlparse
from bs4 import BeautifulSoup

print "\nOceniarka filmów"#.decode('utf8').encode('cp1250')

#odp = "N"
#tablic = []
#tablic.append("1. FilmWeb.pl")
#licz = len(tablic)
#while odp[0] == "N" and licz > 0:
#    for item in tablic:
#        print item
#    try:
#        odpo = int(raw_input('Wybierz oceniacza: '))
#    if odpo < licz:
#            tablic.remove(tablic[odpo-1])
#            licz=licz-1
#    except:
#        print ' '
#    odp = raw_input('Czy to już wszyscy ? [T/N]').upper()

tytul = raw_input('Podaj nazwe filmu: ')
tablica_wynikowa = []
tablica_ocen = []

#FilmWeb.pl
try:
    page_filmweb = "http://www.filmweb.pl/search?q="+tytul
    session = requests.session()
    response = session.get(page_filmweb, verify=False)
    # Odczyt strony
    #if response.json()[u'status'] !='OK':
    #    print "Nieznaleziona linia"
    #    exit()
    html = response.text
    parsed = BeautifulSoup(html)
    grades = parsed.find_all('div', class_='searchResultCol_2_wrapper')
    tablica_wynikowa.append(grades[0].find_all('a', class_='searchResultTitle')[0].text)
    tytul = tablica_wynikowa[0][1:].split('/')[0]
    tablica_wynikowa.append(grades[0].find_all('div', class_='searchResultDetails')[0].text.split(" ")[0])
    tablica_wynikowa.append(str(grades[0].find('div', class_='searchResultRating').text.split(" ")[0]))
    int(tablica_wynikowa[2][0])
    tablica_ocen.append(tablica_wynikowa[2])
    print tablica_wynikowa[0]+":"
    print "  "+tablica_wynikowa[2] + " - FilmWeb.pl"
except:
    print "  -.- - FilmWeb.pl"

#IMDB.com
def imdb(tytul):
    try:
        detalis_of_movie = 'http://www.imdb.com'
        link_to_film = []
        arguments_to_imdb = {'q': tytul, 's': 'all'}
        parsed = BeautifulSoup((requests.get(detalis_of_movie + "/find", params=arguments_to_imdb).text))
        #for item in parsed.find_all(class_="result_text"):
        #    if item.a.text == tytul:
        #        link_to_film.append(item.a['href'])
        link_to_film.append(parsed.find(class_="result_text").a['href'])
        parsed2 = BeautifulSoup((requests.get(detalis_of_movie + link_to_film[0]).text))
        rate = (parsed2.find(itemprop="ratingValue")).text   #tu znajduje sie ocena
        year = (parsed2.find(class_="nobr")).a.text          #tu rok, tytul w 'title'
        tablica_ocen.append(rate)
        print "  "+rate + " - " + "IMDb.com"
    except:
        return 0
        #print "  -.- - IMDb.com"

przypadek = "  -.- - IMDb.com"
if imdb(tytul) == 0:
    try:
        if imdb(tablica_wynikowa[0][1:].split('/')[1]) == 0:
            print przypadek
    except:
        print przypadek

#FDB.pl
def fdb(tytul):
    try:
        detalis_of_movie2 = 'http://fdb.pl/szukaj/index'
        arguments_to_fdb = {'query': tytul}
        link_to_movie = (BeautifulSoup((requests.get(detalis_of_movie2, params=arguments_to_fdb).text))).find(class_="results").a['href']
        rate2 = (BeautifulSoup(requests.get(link_to_movie).text)).find(class_="vote clearfix").big.text
        year2 = (BeautifulSoup(requests.get(link_to_movie).text)).find(id="movie-title").small.a.text
        tablica_ocen.append(rate2)
        print "  "+rate2 + " - " + "FDB.pl"
    except:
        return 0
        #print "  -.- - FDB.pl"

przypadek2 = "  -.- - FDB.pl"
if fdb(tytul) == 0:
    try:
        if fdb(tablica_wynikowa[0][1:].split('/')[1]) == 0:
            print przypadek2
    except:
        print przypadek2

#Wolframalpha.com
#print len(tablica_ocen)
if len(tablica_ocen) > 0:
    page_wolfram = "http://www.wolframalpha.com/input/?i=%28"
    oceny =[]
    for it in tablica_ocen:
        ite = ""
        for znak in it:
            if znak == ',':
                ite = ite + '.'
            else:
                ite = ite + znak
        page_wolfram = page_wolfram+ite+"%2B"
        oceny.append(ite)
    page_wolfram = page_wolfram[:-3]+"%29%2F"+str(len(tablica_ocen))
    #print page_wolfram
    session = requests.session()
    response = session.get(page_wolfram, verify=False)
    html = response.text
    parsed = BeautifulSoup(html)
    grades = parsed.find_all('div', class_='output pnt')
    try:
        area = grades[1].find('area')['href']
        srednia = area.split('=')[1].split('&')[0]
    except:
        srednia=0
        for ini in oceny:
            srednia = float(srednia)+float(ini)
        srednia = float(srednia) / float(len(oceny))
    print " ----\n "+str(srednia)+"\n"
else:
    print " ----\n -.-\n"